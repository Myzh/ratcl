# Makefile for ratcl

PROJECTNAME = ratcl

CMP := npx tsc
FORMAT := npx prettier

OUTPUTDIR := build

GLOBALFLAGS += --strict
GLOBALFLAGS += --alwaysStrict

RELEASEFLAGS += --removeComments
RELEASEFLAGS += --noImplicitThis
RELEASEFLAGS += --noUnusedLocals
RELEASEFLAGS += --noUnusedParameters
RELEASEFLAGS += --noImplicitReturns
RELEASEFLAGS += --noFallthroughCasesInSwitch  
RELEASEFLAGS += --noImplicitReturns  

DEBUGFLAGS += --sourceMap 

--init:
	@echo "\033[0;35mProject: $(PROJECTNAME)\033[0m"

--setup: clean src/ratcl.ts src/ratcl.css
	mkdir -p $(OUTPUTDIR)
	cp -r src $(OUTPUTDIR)

	$(CMP) --version

setup:
	@echo "\033[0;32mSetting up workspace...\033[0m"

	npm install --save-dev typescript
	npm install --save-dev --save-exact prettier

clean: --init
	@echo "\033[0;32mCleaning up...\033[0m"

	rm -rf $(OUTPUTDIR)

pretty: --init
	@echo "\033[0;32mPrettify the code...\033[0m"

	$(FORMAT) --write .

check: --init
	@echo "\033[0;32mChecking for correct code style...\033[0m"

	$(FORMAT) --check .

release: --setup check
	@echo "\033[0;32mBuilding release...\033[0m"

	@for file in `find $(OUTPUTDIR) -name *.ts -print`; do \
		echo Comiling $$file; \
		$(CMP) $(GLOBALFLAGS) $(RELEASEFLAGS) $$file; \
	done

	rm -rf `find $(OUTPUTDIR) -name *.ts -print`

debug: --setup
	@echo "\033[0;32mBuilding debug...\033[0m"

	@for file in `find $(OUTPUTDIR) -name *.ts -print`; do \
		echo Comiling $$file; \
		$(CMP) $(GLOBALFLAGS) $(DEBUGFLAGS) $$file; \
	done
